## cli 工具


## 发布说明

```
$ # 1. 身份验证配置
$ # 此设置包含两部分
$ #   1. projects/[仓库项目id]  如 41290673 为 fat-cli 的项目号
$ #   2. _authToken "项目部署令牌]"  如 MycEyGv8Ts8vQmtKE2A7 为 fat-cli 的部署令牌
$ npm config set -- '//gitlab.com/api/v4/projects/41290673/packages/npm/:_authToken' "MycEyGv8Ts8vQmtKE2A7"
$
$ # 2. 修改代码和版本
$
$ # 3. 发布
$ npm publish
```

## 作为 npm 包使用

```
$ # 1. 设置访问令牌
$ npm config set -- '//gitlab.com/api/v4/packages/npm/:_authToken' "MycEyGv8Ts8vQmtKE2A7"
$
$ # 2. 设置仓库映射
$ npm config set @fatpay:registry https://gitlab.com/api/v4/packages/npm/
$
$ # 2. 安装
$ npm install -g @fatpay/fat-cli
$
$ # 执行命令
$ fat-cli  或者是在 window 环境执行 fat-cli.cmd
```

## 开发新功能步骤

1. 修改代码

2. 编译代码

  编译后会生成 dist 目录

  ```
  npm run build
  ```
  
3. 调试新功能

  可以采用 npm link 方式调试编译好的代码
  
  ```
  # 执行 npm link 把命令链接到全局
  npm link
  ```
  
  该命令只需执行一次，因为使用了链接，所以下次编译代码，命令会自动生效

  执行完后，我们就得到了一个全局命令 `fat-cli`
  
4. 调试命令

  找一个空目录执行上述链接的 `fat-cli` 命令，如：
  
  ```
  # 假设当前在 D:/app 目录
  $ fat-cli
  ```

  根据运行结果验证程序运行正确性
