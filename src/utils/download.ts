const download = require('download-git-repo')

export default (template: string, to: string, options?: any) => {
  return new Promise((resolve): void => {
    download(template, to, options, (err: any) => {
      if (err) {
        resolve(err)

      } else {
        resolve(null)
      }
    })
  })
}
