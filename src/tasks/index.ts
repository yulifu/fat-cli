import init from './init'
import component from './component'

export {
  init,
  component
}
