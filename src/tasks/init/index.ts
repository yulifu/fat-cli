const { existsSync, writeFile, readFile } = require('node:fs')
const path = require('node:path');

import downloadTemplate from '../../utils/download'
import { TEMPLATE_URL } from '../../configs/index'
import { start, stop } from '../../utils/loading'
import { confirm, input, log } from '../../utils/msg';

const doConfirm = (name: string, to: string) => {
  confirm({
    title: 'The project already exists. Do you want to continue?',
    callback: (answer: boolean) => {
      if(answer) {
        clone(name, to)
      }
    }
  })
}

const clone = async (project: string, to: string): Promise<void> => {
  start()
  const rs = await downloadTemplate(TEMPLATE_URL, to, {clone: true})
  stop()

  if(rs !== null) {
    console.error(rs)
    return
  }

  readFile(to + '/package.json', {encoding: 'utf-8'}, (err: any, data: string) => {
    if (err) {
      console.error(err)
    } else {
      const ret = data.replace('{{projectName}}', project)
      writeFile(to + '/package.json', ret, (err: any) => {
        if (err) throw err
        console.log('Done!')
      })
    }
  })
}

const create = (projectName: string = ''): void => {
  const toDir = path.join(process.cwd(), projectName)
  const needConfrim = existsSync(toDir)

  if(needConfrim) {
    doConfirm(projectName, toDir)

  } else {
    clone(projectName, toDir)
  }
}

export default () => {
  input({
    name: 'projectName',
    title: 'What is the project name you want?',
    callback: (obj: any) => {
      if(!obj.projectName) {
        log('The project name is missing!', 'red')

      } else {
        create(obj.projectName)
      }
    }
  })
}
