const template =
`<template>
  <div class="wrapper"></div>
</template>

<script lang="ts" setup>
defineOptions({
  name: '{{name}}'
})
</script>

<style lang="less">
.wrapper {}
</style>`

export default template
