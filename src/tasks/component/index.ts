const process = require('node:process')
const { existsSync, writeFile, readdir, statSync } = require('node:fs')
// const { Select } = require('enquirer')
const Handlebars = require("handlebars")
import { confirm, input, select, log } from '../../utils/msg'
import { nameToHyphen } from '../../utils/string'
import { createDirectory } from '../../utils/file'
import template from './template'

const createComponent = (path: string, componentName: string) => {
  const dir = path + '/' + componentName

  createDirectory(dir, () => {
    const data = Handlebars.compile(template)({name: componentName})

    writeFile(dir + '/index.ts', data, (err: any) => {
      if (err) {
        console.error(err)
      } else {
        console.log('Done, add component success')
      }
    });
  })
}

const addComponentToProject = (projectRootDir: string) => {
  let path = projectRootDir + '/src/components'

  input({
    name: 'comname',
    title: 'What is the component name you want?',
    callback: (obj: any) => {
      if(!obj.comname) {
        log('The component name is missing!', 'red')

      } else {
        let comname = nameToHyphen(obj.comname)
        if(existsSync(path + '/' + comname)) {
          log('The component already exists!', 'red')

        } else {
          createComponent(path, comname)
        }
      }
    }
  })
}

const confirmProject = (projectRootDir: string) => {
  const name = require(projectRootDir + '/package.json').name

  confirm({
    title: 'Do you want to add component to project: ' + name,
    callback: (rs: boolean) => {
      if(rs) {
        addComponentToProject(projectRootDir)
      }
    }
  })
}
const selectProject = (currentDir: string) => {
  readdir(currentDir, { withFileTypes: true }, (err: any, list: any) => {
    let ret = list.filter((item: any) => item.isDirectory())

    ret = ret.map((folder: any) => {
      const fullFolderPath = currentDir + '/' + folder.name
      const stats = statSync(fullFolderPath);
      return { name: folder.name, path: fullFolderPath, ctimeMs: stats.ctimeMs }
    })

    ret.sort((a: any, b: any) => {
        return b.ctimeMs - a.ctimeMs;
    })


    if(ret.length <= 0) {
      log('you must init a project first!', 'red')
      return
    }

    select({
      title: 'Which project do you want to add to?',
      list: ret.map((item: any) => item.name),
      callback: (rs: string) => {
        const dir = currentDir + '/' + rs

        addComponentToProject(dir)
      }
    })
})
}

export default () => {
  // 是否已经在项目目录了
  const current = process.cwd()
  const inProject = existsSync(current + '/package.json')

  if(inProject) {
    confirmProject(current)

  } else {
    selectProject(current)
  }
}
